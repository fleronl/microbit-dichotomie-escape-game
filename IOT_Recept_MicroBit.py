# coding: utf-8
"""
laura.fleron@ac-reims.fr
ver : 1
09/12/2021
"""
from microbit import *
from random import randint, choice
import music

uart.init(baudrate=115200, bits=8, parity=None, stop=1) # , tx=pin0, rx=pin1)
sleep(50)
cadenas = (7513, 9513, 2486)
valeur = 0
cycle = 0

none = Image("00550:"
             "05005:"
             "00050:"
             "00500:"
             "00500")

def Send(m):
    """
    envoi message sup, inf, ou egal
    """
    display.scroll(m)
    sleep(50)
    d = bytes(m, 'utf-8')
    uart.write(d)

def Receive(cycle):
    """
    Reception du prog python d'une valeur
    pour le code
    """
    if uart.any():
        m = uart.readline(4)
        sleep(50)
        s = ""
        for b in m:
            s = s + chr(b)

        display.show(Image.ARROW_S)
        sleep(250)

        cycle += 1
        display.scroll(str(cycle))
        sleep(50)

        return  int(s), cycle

    else : return 0, cycle

def Alea(cadenas):
    """
    Code: la valeur à trouver pour obtenir le cadenas
    cadenas: valeur d'un des trois cadenas donnée si le code est bon
    """
    codeAlea = randint(1000,9999)
    codecadenas = choice(cadenas)
    #display.scroll(codeAlea)
    sleep(50)
    return (codeAlea, codecadenas)

def blocage():
    """
    Blocage de la carte après 15 tentatives
    """
    while 1:
        display.show(Image.ANGRY)

code, codecadenas = Alea(cadenas)

#--------------------------------Code principal

while valeur != code :

    valeur, cycle = Receive(cycle)
    sleep(50)
    #display.scroll(valeur)

    if valeur == 0:
        display.show(none)

    else :
        if valeur < code :
            Send('<')
        elif valeur > code :
            Send('>')
        else :
            Send('=')

    if button_a.is_pressed():
        uart.write(b'1234')
        display.show(Image.HAPPY)
        sleep(500)

    if cycle == 15: blocage()

music.play(music.PYTHON)
display.scroll(codecadenas)


