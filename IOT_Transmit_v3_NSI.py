# coding: utf-8
"""
laura.fleron@ac-reims.fr
version 1
le 08/12/2021
Version pour les 1er NSI
"""
import serial, time
from TestSerialPort_V2 import testPortSerie
   
def transmission(milieu):
    #print(f'Envoi de : {milieu}')
    message = bytes(milieu, 'utf-8')
    sp.write(message)

def reception():
    #print("Reception")
    data = sp.read()
    data = chr(data[0])
    return data

def rechercherCode(mini, maxi):
    """
    Mettre ici votre code ...
    fonction prennant pour argument le code mni et le code maxi
    retourne un tuplet (la valeur trouvée, le nbr d'essai)
    """
    code, essai = '0000', 0
    return (code, essai)
        
if __name__ == "__main__":
    
    print("Recherche du port de communication :")
    port, baudrate, bytesize, parities, stopbits = testPortSerie()
    sp = serial.Serial()
    sp.port = port
    sp.baudrate = baudrate
    sp.bytesize = bytesize
    sp.parities = parities
    sp.stopbits = stopbits

    try:
        sp.open()

    except:
        print("\n!!!!!!!!!!!!!!! Carte Micro:bit introuvable !!!!!!!!!!!!!!!!!")

    else:
        print("\nDemarrage de la recherche...")
        maxi = 2
        mini = 1
        
        leCode, essai = rechercherCode(mini, maxi)
        
        print('\n---------------------------------------------')
        print(f'| Code {leCode} trouvé au bout de {essai} tentatives |')
        print('---------------------------------------------')
        sp.close()
                
    finally:
        print("\nArrêt du programme.")
