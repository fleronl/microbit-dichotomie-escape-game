# coding: utf-8
"""
laura.fleron@ac-reims.fr
version 1
le 08/12/2021
version pour la classe de SNT
"""
import serial, time
from TestSerialPort_V2 import testPortSerie
   
def transmission(milieu):
    message = bytes(milieu, 'utf-8')
    sp.write(message)

def reception():
    data = sp.read()
    data = chr(data[0])
    return data
"""
def rechercherCode(mini, maxi):
    essai = 0
    
    while mini != maxi :
        essai += 1
        print('----------------------')
        print(f'{essai} : {mini} < code < {maxi}')
        #Calcule la valeur 'milieu' entre la valeur mini et la valeur maxi
        transmission(str(milieu))
        resultat = reception()

        #Si la valeur resultat gal à '>' faire
                # maxi égal au milieu
                print(f'   {milieu} est trop grand')
                
        #Sinon si la valeur resultat égal à '<' faire
            #mini égal au milieu
            print (f'   {milieu} est trop petit')
            
        #Sinon
            #mini égal à maxi égal à milieu

    return (milieu, essai)
"""        

if __name__ == "__main__":
    
    print("Recherche du port de communication :")
    port, baudrate, bytesize, parities, stopbits = testPortSerie()
    sp = serial.Serial()
    sp.port = port
    sp.baudrate = baudrate
    sp.bytesize = bytesize
    sp.parities = parities
    sp.stopbits = stopbits

    try:
        sp.open()

    except:
        print("\n!!!!!!!!!!!!!!! Carte Micro:bit introuvable !!!!!!!!!!!!!!!!!")

    else:
        print("\nDemarrage de la recherche...")
        maxi = 9999
        mini = 1000
        """
        leCode, essai = rechercherCode(mini, maxi)
        
        print('\n---------------------------------------------')
        print(f'| Code {leCode} trouvé au bout de {essai} tentatives |')
        print('---------------------------------------------')
        sp.close()
        """    
    finally:
        print("\nArrêt du programme.")
