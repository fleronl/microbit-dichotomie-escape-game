# -*- coding: latin-1 -*-
#---------------------------------------------------------
# Test des diff�rents num�ro de port
# Passage des valeurs vers les autres fichiers Python
# laura.fleron@ac-reims.fr 05/12/2021
#---------------------------------------------------------

import serial				                        #importe le module serial pour communiquer avec la voie s�rie

def testPortSerie():
    se = serial.Serial() 			                #d�finition de la variable -se- communiquant avec le port s�rie
    se.baudrate = 115200	        	                #s�lection de la vitesse de transmission � 4800 bauds
    se.bytesize = 8 				                #Transmission des informations sur 8 bits
    se.parities = 0 				                #sans parit�
    se.stopbits = 1 				                #bit de stop 1
    se.xonxoff = 0 				                #pas de contr�le logiciel
    #se.rtscts = 0 				                #pas de contr�le RTS/CTS
    #se.timeout = 1 				                #timeout � la lecture 
    #se.port = nPort 				                #le port com n-1 car on commence � 0
    se.writeTimeout = 1 			                #timeout � l �criture

    for nPort in range(1, 12):

        se.port = 'COM' + str(nPort)                            #Configuration du num du port
        flag = 0
        
        try:
            se.open()                                           #ouverture du port

        except:
            flag = se.isOpen()                                  #Variable true ou false, port ouvert ou non
            print(f'Port COM{nPort} : {flag}')
            portOpe = "COM??"

        else:
            flag = se.isOpen()                                  #Variable true ou false, port ouvert ou non
            print(f'--> Port COM{nPort} : {flag} <--')
            se.close()
            portOpe = 'COM' + str(nPort)
            break

    return (portOpe, se.baudrate, se.bytesize, se.parities, se.stopbits)

if __name__ == "__main__":

    print(testPortSerie())
    
