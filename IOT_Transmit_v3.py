# coding: utf-8
"""
laura.fleron@ac-reims.fr
version 1
le 08/12/2021
Proposition de corrigé
"""
import serial, time
from TestSerialPort_V2 import testPortSerie
   
def transmission(milieu):
    #print(f'Envoi de : {milieu}')
    message = bytes(milieu, 'utf-8')
    sp.write(message)

def reception():
    #print("Reception")
    data = sp.read()
    data = chr(data[0])
    return data

def dichotomie(mini, maxi):
    essai = 0
    
    while mini != maxi :
        essai += 1
        print('----------------------')
        print(f'{essai} : {mini} < code < {maxi}')
        milieu = (maxi + mini) // 2
        transmission(str(milieu))
        resultat = reception()

        if resultat == '>':
                maxi = milieu
                print(f'   {milieu} est trop grand')
                
        elif resultat == '<' :
            mini = milieu
            print (f'   {milieu} est trop petit')
            
        else :
            mini = maxi = milieu

    return (milieu, essai)
        
if __name__ == "__main__":
    
    print("Recherche du port de communication :")
    port, baudrate, bytesize, parities, stopbits = testPortSerie()
    sp = serial.Serial()
    sp.port = port
    sp.baudrate = baudrate
    sp.bytesize = bytesize
    sp.parities = parities
    sp.stopbits = stopbits

    try:
        sp.open()

    except:
        print("\n!!!!!!!!!!!!!!! Carte Micro:bit introuvable !!!!!!!!!!!!!!!!!")

    else:
        print("\nDemarrage de la recherche...")
        maxi = 9999
        mini = 1000
        
        leCode, essai = dichotomie(mini, maxi)
        
        print('\n---------------------------------------------')
        print(f'| Code {leCode} trouvé au bout de {essai} tentatives |')
        print('---------------------------------------------')
        sp.close()
                
    finally:
        print("\nArrêt du programme.")
